# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import redirect,render
from django.shortcuts import render
import requests
import json
import time
import threading, time
import datetime
import pytz
# Create your views here.
from django.http import HttpResponse

coins = ['btc_usdt','bch_usdt','eth_usdt','etc_usdt','qtum_usdt','ltc_usdt','dash_usdt','zec_usdt','eos_usdt']
huobidata = []
gatedata = []
def index(request):
    threading.Thread(target=gate_ticker).start()
    return render(request,'index.html')

def ticker(request):
      res = []
      for data in gatedata:
          for hdata in huobidata:
              if data['cur'] == hdata['cur']:
                  profit = False
                  h_price = float(hdata['tick']['ask'][0])
                  g_price = float(data['last'])
                  try:
                    expire_datetime =  datetime.datetime.fromtimestamp(int(hdata['ts'])/1000,pytz.timezone('Asia/Shanghai'))
                    expire_time =  expire_datetime.strftime("%Y-%m-%d-%H-%M-%S")
                  except Exception as e:
                      print(e)
                  t_hdata = hdata
                  t_hdata['date'] = expire_time
                  if (h_price - g_price)/g_price >= 0.01 :
                      profit = True
                  res.append({'gate':data,'huobi':t_hdata,'profit':int(profit)})
      return HttpResponse(json.dumps(res))

def gate_ticker():
    gateurl = "https://api.huobi.pro/market/detail/merged?symbol="
    res = []

    try:
        for coin in coins:
            origincoin = coin.replace('_','')
            cur_url = gateurl+origincoin
            response = requests.get(cur_url)
            obj = json.loads( response.text)
            obj['cur'] = coin
            res.append(obj)
            time.sleep(0.1)
        global huobidata
        huobidata = res
    except Exception as e:
        print(e)
    try:
        response = requests.get("http://data.gate.io/api2/1/tickers")
        global gatedata
        gateres = json.loads(response.text);
        gatestore = []
        for key, value in gateres.iteritems():
            if key in coins:
                value['cur'] = key
                gatestore.append(value)
        gatedata = gatestore
    except Exception as e:
        print(e)
    gate_ticker()


if __name__ == "__main__":
     a = 1
     c = a
     d = a
     f = [a,34,56,78]
     print(f)
     a = 2
     print(f)